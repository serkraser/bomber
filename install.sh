#!/bin/bash

apt-get update
apt-get -y install python3-pip
apt-get -y install openvpn
apt-get -y install net-tools
pip3 install -r attacker/requirements.txt
ip route add  $(echo $SSH_CLIENT | awk '{ print $1}') via $(route -n | grep 'UG[ \t]' | awk '{print $2}')
if [ ! -e /etc/systemd/system/script.service ]; then
  echo "[Unit]
Description=My Script
After=network.target 

[Service]
Type=forking
ExecStart=/root/bomber/script.sh
User = root
Group = root
Restart = on-failure
SyslogIdentifier = myServic
RestartSec = 10
TimeoutStartSec = infinity

[Install]
WantedBy=multi-user.target" >> /etc/systemd/system/script.service
systemctl daemon-reload
systemctl enable script
fi

line="*/30 * * * * sudo reboot"
(crontab -u $(whoami) -l; echo "$line" ) | crontab -u $(whoami) -
#systemctl enable cron

reboot
